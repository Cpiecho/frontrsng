import { Component, OnInit,DoCheck } from '@angular/core';
import {UserService} from './services/user.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {UserEditComponent} from './components/user-edit/user-edit.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers:[UserService]
})
export class AppComponent implements OnInit,DoCheck{
  public title: string;
  public identity;

  constructor(
    private _userService:UserService,
    private _route: ActivatedRoute,
    private _router: Router
  ){
    this.title = 'FitPeople';
  }

  ngOnInit(){
    this.identity=this._userService.getIdentity();
    console.log(this.identity);
  }

  ngDoCheck(){
    this.identity=this._userService.getIdentity();
  }

  logout(){
    localStorage.clear();
    this._router.navigate(['/']);
  }
}
