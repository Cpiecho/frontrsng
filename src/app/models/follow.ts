export class Follow{
    constructor(
        public user: string,
        public follower: string
    ){}
}