export class User{

    constructor(
        public name: string,
        public surName: string,
        public nickName: string,
        public email: string,
        public password: string,
        public role: string,
        public image: string
    ){

    }
}