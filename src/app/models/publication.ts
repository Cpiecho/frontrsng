export class Publication{
    constructor(
        public id: string,
        public idReferenced: string,
        public text: string,
        public urlFile: string,
        public created_at: string,
    ){}
}