  import { Component, OnInit } from '@angular/core';
  import { Router, ActivatedRoute, Params } from '@angular/router';
  import { User } from '../../models/user';
  import { UserService } from '../../services/user.service';

  @Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.scss'],
    providers: [UserService]
  })

  export class UserEditComponent implements OnInit {

    public title: string;
    public user: User;
    public identity;
    public token;
    public status: string;

    constructor(
      private _route: ActivatedRoute,
      private _router: Router,
      private _userService: UserService
    ) {
      this.title = 'Configuracion de usuario';
      this.user = this._userService.getIdentity();
      this.identity = this.user;
      this.token = this._userService.getToken();
    }

    ngOnInit(): void {
      console.log('Componente user-edit cargado');
      console.log(this.user);

    }
      onSubmit() {
        this._userService.updateUser(this.user).subscribe(
          response => {
              this.status = 'success';
              localStorage.setItem('identity', JSON.stringify(this.user));
              this.identity = this.user;
              /* 
              if(this.filesToUpload != null){
                this._uploadService.makeFileRequest(this.url+'upload-image-user/'+this.user._id, [], this.filesToUpload, this.token, 'image').then((result: any) => {
                  //console.log(result);
                  this.user.image = result.user.image;
                  localStorage.setItem('identity', JSON.stringify(this.user));
                });
              }*/
          },
          error => {
            var errorMessage = <any>error;
            if(errorMessage != null){
              this.status = 'error';
            }
          }
          );
      }


    public filesToUpload: Array<File>;
    fileChangeEvent(fileInput: any){
      this.filesToUpload = <Array<File>>fileInput.target.files;
    }
  }