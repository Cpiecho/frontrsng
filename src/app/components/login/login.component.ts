import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {
  public title: string;
  public user: User; 
  public status: string;
  public identity;
  public token;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ){
    this.user = new User("","","","","","","");
    this.title = 'Identificate';
  }

  ngOnInit(): void {
    console.log('Login cargado')
  }
  onSubmit(){
    console.log(this.user);
    this._userService.signup(this.user).subscribe(
      response => {
        this.identity = new User(response.name.string,response.surName.string,response.nickName.string,response.email.string,response.password.string,response.role.string,response.image.string);
        this.token = response.token.string;
        //Damos persistencia a los datos, almacenandolos en localStorge
        localStorage.setItem('identity',JSON.stringify(this.identity));
        localStorage.setItem('token',this.token);
        this.getCounters();
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if(errorMessage != null){
          this.status = 'error';
        }
      }
    );
  }

  getCounters(){
    this._userService.getCounters(this.user.nickName).subscribe(
      response => {
        localStorage.setItem('stats', JSON.stringify(response));
        console.log(response);
        this.status = 'success';
        this._router.navigate(['/']);
    },
    error => {
        console.log(<any>error);
    }
    );
  }
}
